# Wardrobify

Team:

* Constance Basco - Hats microservice
* Person 2 - Which microservice?

## Design

## Shoes microservice - Amanda Southern

Explain your models and integration with the wardrobe
microservice, here.

The Shoe Model interacts with the BinVO as a Foreign Key. Through Polling, the poller interacts with the wardrobe API microservice 
to get the information on what bins there are in the microservive. Through an BinVO model encoder, the ShoeEncoder is able to relay 
the information in the BinVO to the shoes_rest views. 

## Hats microservice - Constance Basco

Explain your models and integration with the wardrobe
microservice, here.
