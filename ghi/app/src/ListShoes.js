import { Link } from 'react-router-dom';
import React, { useEffect, useState} from 'react';



function ShoesList(props){

  const [shoes, setShoes] = useState('')

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
      
      
    }
  }
  useEffect(() => {
    fetchData();
    
    
  }, []);

  async function handleDelete(id){
    
    const response = await fetch(`http://localhost:8080/api/shoes/${id}/`,{
      method: 'DELETE'
    })

    if (response.ok){
      fetchData()
      window.location.reload()
    }
    
    
  }
    return (
        <>
        <div>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add a shoe</Link>
          </div>
            <table className="table table-striped">
            <thead>
              <tr>
                <th>Manufacturer</th>
                <th>Model Name</th>
                <th>Color</th>
                <th>Picture</th>
                <th>Bin</th>              
              </tr>
              </thead>
            <tbody>          
            {props.shoes?.map(shoe => {
              return (
                <tr key={shoe.id} className="mb-3 shadow">
                  <td>{ shoe.manufacturer }</td>
                  <td>{ shoe.model_name }</td>
                  <td>{ shoe.color }</td>
                  <td><img src={ shoe.picture_url } className="img-thumbnail"/></td>
                  <td>{ shoe.bin.closet_name }</td>                  
                  <td>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                    <button type="button" class="btn btn-outline-danger" onClick={() => {handleDelete(shoe.id)}}>Delete</button>
                    </div>
                  </td>                  
                </tr>
              );
            })}
              </tbody>
              </table>
        </div>
      </>
      );
    }

export default ShoesList
