import React, { useEffect, useState } from 'react';

function HatsForm(props) {

  // Set the useState hook to store "name" in the component's state,
  // with a default initial value of an empty string.
  const [fabric, setFabric] = useState('');

  // Create the handleNameChange method to take what the user inputs
  // into the form and store it in the state's "name" variable.
  const handleFabricChange = (event) => {
    const value = event.target.value;
    setFabric(value);
  };

  const [style, setStyle] = useState('');

  const handleStyleChange =(event) => {
    const value = event.target.value;
    setStyle(value);
  }

  const [color, setColor] = useState('');

  const handleColorChange =(event) => {
    const value = event.target.value;
    setColor(value);
  }

  const [pictureUrl, setPictureUrl] = useState('');

  const handlePictureUrlChange =(event) => {
    const value = event.target.value;
    setPictureUrl(value);
  }

  const [locations, setLocations] = useState([]);
  const [location, setLocation] = useState('');
  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleLocationChange =(event) => {
    const value = event.target.value;
    setLocation(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.fabric = fabric;
    data.style = style;
    data.color = color;
    data.picture_url = pictureUrl;
    data.location = location;


    const HatsUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    }

  // fetchData, useEffect, and return methods...
    const response = await fetch(HatsUrl, fetchConfig);
      if (response.ok) {
        const newHat = await response.json();

        setFabric('');
        setStyle('');
        setColor('');
        setPictureUrl('');
        setLocation('');
      }





  }

  return (
    <>
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hats-form">
            <div className="form-floating mb-3">
              <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStyleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
              <label htmlFor="style">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input  onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input  onChange={handlePictureUrlChange} placeholder="Picture" required type="text" name="pictureUrl" id="pictureUrl" className="form-control" />
              <label htmlFor="pictureUrl">Picture Url</label>
            </div>
            <div className="mb-3">
              <select onChange={handleLocationChange} required name="location" id="location" value={location} className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.import_href} value={location.import_href}>
                      {location.closet_name}
                    </option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    </>
  );
}

export default HatsForm;
