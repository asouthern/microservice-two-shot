import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ListShoes';
import ShoeForm from './ShoeForm';
import HatsList from './ListHats';
import HatsForm from './HatForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes/" element={<ShoesList shoes={props.shoes} />} />
          <Route path='shoes/new' element={<ShoeForm />} />
          <Route path="hats/" element={<HatsList hats={props.hats} />} />
          <Route path="new" element={<HatsForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
