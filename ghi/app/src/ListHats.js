import { Link } from 'react-router-dom';

function HatsList(props) {
    return (
        <>
      <div>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
          <Link to="/new" className="btn btn-primary btn-lg px-4 gap-3">Add a hat!</Link>
        </div>
        <table>
          <thead>
            <tr>
              <th>Fabric</th>
              <th>Style</th>
              <th>color</th>
            </tr>
          </thead>
          <tbody>
            {props.hats.map(hat => {
                return (
                    <tr key={hat.href}>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.style }</td>
                        <td>{ hat.color}</td>
                        <img src= { hat.picture_url } />
                    </tr>
                );
            })}
          </tbody>
        </table>
      </div>
      </>
    );
  }

  export default HatsList;
