import React, { useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';


function ShoeForm(props){
    const [bins, setBins] = useState([])
    const [manufacturer, setManufacturer] = useState('')
    const [color, setColor] = useState('')
    const [modelName, setModelName] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [bin, setBin] = useState('')
    const navigate = useNavigate()


    const fetchData = async () => {
      const url = 'http://localhost:8100/api/bins/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setBins(data.bins);
      }
    }
  
    useEffect(() => {
      fetchData();
    }, []);

    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)

    }

    const handleModelNameChange = (event) => {
        const value = event.target.value
        setModelName(value)
    }

    const handlePictureUrl = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }

    const handleBinChange = (event) => {
      const value = event.target.value
      setBin(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin


        const shoeUrl =  "http://localhost:8080/api/shoes/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
          }

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();

        setManufacturer('');
        setModelName('');
        setColor('');
        setPictureUrl('');
        setBin('')

        navigate('/shoes')
        window.location.reload()
        
      } else{
        console.log('did not submit')
        console.log(data)
      }


    }  

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new shoe</h1>
              <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} placeholder="Manufacturer" required
            type="text" value={manufacturer} name="manufacturer" id="manufacturer"
                className="form-control" />
                  <label htmlFor="manufacturer">Manufacturer Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={handleColorChange} placeholder="Color" required type="text" value={color} name="color" id="color" className="form-control"/>
                      <label htmlFor="color">Color</label>
                    </div>
                      <div className="form-floating mb-3">
                        <input onChange={handleModelNameChange} placeholder="Model Name" required type="text" value={modelName} name="model_name" id="model_name" className="form-control"/>
                        <label htmlFor="modelName">Model Name</label>
                      </div>
                        <div className="form-floating mb-3">
                        <input onChange={handlePictureUrl} placeholder="Picture Url" required type="url" value={pictureUrl} name="picture_url" id="picture_url" className="form-control"/>
                        <label htmlFor="pictureUrl">Picture Url</label>
                        <div className="mb-3">
                    <select onChange={handleBinChange} required id="bin" name="bin" value={bin} className="form-select">
                      <option value="">Choose a bin</option>
                      {bins.map(bin=> {
                        return (
                      <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                        )
                      })}
                    </select>
                </div>
                </div>
               <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
    )
    
}

export default ShoeForm
