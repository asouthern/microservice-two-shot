from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import  Hats,LocationVO

# Create your views here.
class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
    ]

class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style",
        "picture_url",
    ]



class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "fabric",
        "style",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }



@require_http_methods(["GET","POST"])
def api_list_hat(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats":hats},
            encoder=HatsListEncoder
        )
    else:
        content = json.loads(request.body)
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )



@require_http_methods(["GET","POST"])
def api_list_hats(request,location_vo_id=None):
    if request.method == "GET":
        hats = Hats.objects.filter(location=location_vo_id)
        return JsonResponse(
            {"hats":hats},
            encoder=HatsListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = f'/api/locations/{location_vo_id}/'
            location = LocationVO.objects.get(import_href = location_href)

            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False,
        )
